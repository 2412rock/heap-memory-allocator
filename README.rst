A heap allocator similar to ptmalloc2, jemalloc,
tcmalloc, and many others. These allocators are the underlying code of malloc. Heap allocators
request chunks of memory from the operating system and place several (small) object inside these. Using
the free call these memory objects can be freed up again, allowing for reuse by future malloc calls.
Important performance considerations of a heap allocator include being fast, but also to reduce memory
fragmentation.
This allocator implements its own malloc, free and realloc functions, and does not use the
standard library versions of these functions. This allocator uses only the brk(2) and sbrk(2)
functions, which ask the kernel for more heap space.