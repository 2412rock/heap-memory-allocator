/*
 * This is where the implementation of the allocator starts
 * Authors:
 *  Adrian Albu <a.albu@vu.nl>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

typedef struct metadata
{
    size_t size;
    struct metadata *next;
    short free;
    int free_number;
} meta;


meta *first;
size_t free_bytes = 0;
size_t bytes_alloced = 0;
int number_of_free = 0;

int is_alligned(size_t size)
{
    if (size % 8 == 0)
    {
        return 1;
    }
    return 0;
}

void *can_be_merged(size_t req_size)
{
    meta *it = first;
    size_t size = 0;
    meta *block_ends_here = NULL; //38
    meta *block_starts_here = NULL;
    int current_index = 0;
    int last_free_index = -42;
    while (it != NULL)
    {
        if (it->free == 1)
        {
            if (block_ends_here == NULL)
            {
                block_ends_here = it;
                size += block_ends_here->size + sizeof(meta); //last block doesnt use the metadata so we add it to the final size
                //printf("Block end here -> %p (metadata skipped) \n", (meta *)block_ends_here + 1);
                last_free_index = current_index; //keeps track of continous blocks
            }
            else if (block_ends_here != NULL && block_starts_here == NULL && last_free_index + 1 == current_index) //itial start setupt
            {
                //printf("Initial block start setup \n");
                block_starts_here = it;
                size += block_starts_here->size; //we need the metadata of the block so we dont add it to the final size
                last_free_index = current_index; //keeps track of continous blocks
                if (size >= req_size)
                {
                    //printf("We have enough consecutive blocks to merge \n");
                    block_starts_here->next = block_ends_here->next;
                    free_bytes -= req_size;                                             
                    return (meta *)block_starts_here + 1;
                }
            }
            else if (block_starts_here != NULL)
            {
                if (last_free_index + 1 == current_index)
                {
                    //last index was free and current index is also free
                    size += it->size + sizeof(meta); //sizeofmeta added from initial start setup, because that was not the actual start
                    block_starts_here = it;
                    if (size >= req_size)
                    {
                        //printf("We have enough consecutive blocks to merge \n");
                        block_starts_here->next = block_ends_here->next; //ot this?
                        free_bytes -= req_size;
                        return (meta *)block_starts_here + 1;
                    }
                    else
                    {
                        //printf("Size is still not enough (%lu bytes), we have to keep searching \n", size);
                        last_free_index = current_index;
                    }
                }
                else
                {
                    //printf("Block is not consecutive, resetting search \n");
                    block_ends_here = it;
                    size = block_ends_here->size + sizeof(meta);
                    block_starts_here = NULL;
                    last_free_index = current_index;
                }
            }
        }
        it = it->next;
        current_index++;
    }
    return NULL;
}

void allign_size(size_t *total_size)
{
    while (1)
    {
        if ((*total_size) % 8 == 0)
        {
            return;
        }
        (*total_size)++;
    }
}

void *mymalloc(size_t req_size)
{
    size_t split_req_size = req_size;
    allign_size(&split_req_size);
    bytes_alloced += req_size;
    size_t total_size = sizeof(meta) + req_size;
    allign_size(&total_size);
    meta *new_mem;

    if (first == NULL)
    {
        void *block = sbrk(total_size); //expands the current memory by size
        new_mem = (meta *)block;
        new_mem->free = 0;
        new_mem->next = NULL;
        new_mem->size = req_size;
        first = new_mem;
    }
    else if (first != NULL)
    {
        // do not forget to substract used bytes in IN MERGE!!!!!
        meta *it = first;

        if (free_bytes >= req_size)
        {
            //////printf("(?) There may be an empty spot in list that can be used, free bytes %lu requested bytes %lu \n", free_bytes, req_size);
            int index = 0;
            meta *l = NULL;
            void *r;
            int biggest_number = -1;
            while (it != NULL)
            {
                if (it->free == 1 && it->size > req_size && split_req_size + sizeof(meta) + 1 <= it->size)
                {
                    void *ptr = (void *)it;
                    void *n = (void *)ptr + split_req_size + sizeof(meta); // to get to meta region in next pointer

                    meta *next_memory = (meta *)n;
                    next_memory->next = it->next;
                    size_t new_size = it->size - split_req_size - sizeof(meta);

                    next_memory->size = new_size;
                    next_memory->free = 1;
                    next_memory->free_number = number_of_free;
                    number_of_free++;
                    it->free = 0;
                    it->size = split_req_size;
                    it->next = next_memory;
                    free_bytes -= split_req_size + sizeof(meta);

                    return (meta *)it + 1;
                }

                else if (it->free == 1 && it->size >= req_size)
                {
                    if (biggest_number == -1)
                    {
                        biggest_number = it->free_number;
                        l = it;
                    }
                    else if (it->free_number > biggest_number)
                    {
                        l = it;
                        biggest_number = it->free_number;
                    }
                }
                it = it->next;
                index++;
            }
            if (l != NULL)
            {
                l->free = 0;
                free_bytes -= l->size;
                r = (meta *)l + 1;
                return r;
            }
        }
        if (it == NULL)
        {
            void *res = can_be_merged(req_size);
            if (res != NULL)
            {
                return res;
            }
        }

        void *block = sbrk(total_size); //expands the current memory block by size 

        new_mem = (meta *)block;
        new_mem->free = 0;
        new_mem->size = req_size;
        meta *temp = first;
        first = new_mem;
        first->next = temp;
    }

    void *r = (meta *)new_mem + 1;

    return r;
}

void *mycalloc(size_t nmemb, size_t size)
{
    void *str = mymalloc(nmemb * size);
    return memset(str, 0, nmemb * size);
}

void myfree(void *ptr)
{
    meta *req_block = (meta *)ptr - 1;
    meta *it = first;
    meta *obj = (meta *)ptr - 1;
    void *current_break = sbrk(0);

    if ((void *)ptr + obj->size == current_break)
    {
        free_bytes += it->size;
        number_of_free++;

        if (free_bytes + obj->size == bytes_alloced)
        {
            sbrk(0 - number_of_free * sizeof(meta) - free_bytes);
            return;
        }
    }

    while (it != NULL)
    {
        if (it == req_block)
        {
            it->free = 1;
            free_bytes += it->size;
            req_block->free_number = number_of_free;
            number_of_free++;
            return;
        }
        it = it->next;
    }
}

void *myrealloc(void *ptr, size_t size)
{
    if (ptr == NULL)
    {
        return mymalloc(size);
    }

    meta *reqested_block = (meta *)ptr - 1;

    if (size == reqested_block->size && ptr != NULL)
    {
        return ptr;
    }
    else if (ptr != NULL)
    {
        if (size < reqested_block->size)
        {
            return ptr;
        }
        void *new_ptr = mymalloc(size);

        void *r = memcpy(new_ptr, ptr, size);
 
        return r;
    }

    return NULL;
}

/*
 * Enable the code below to enable system allocator support for your allocator.
 * Doing so will make debugging much harder (e.g., using ////////printf may result in
 * infinite loops).
 */
#if 1
void *malloc(size_t size) { return mymalloc(size); }
void *calloc(size_t nmemb, size_t size) { return mycalloc(nmemb, size); }
void *realloc(void *ptr, size_t size) { return myrealloc(ptr, size); }
void free(void *ptr) { myfree(ptr); }
#endif
